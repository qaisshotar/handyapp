/*
 Navicat Premium Data Transfer

 Source Server         : Flowers
 Source Server Type    : MySQL
 Source Server Version : 100237
 Source Host           : theflowersandgiftsshop.com:3306
 Source Schema         : flowers_alhaj

 Target Server Type    : MySQL
 Target Server Version : 100237
 File Encoding         : 65001

 Date: 05/04/2021 23:28:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for handyman_user
-- ----------------------------
DROP TABLE IF EXISTS `handyman_user`;
CREATE TABLE `handyman_user`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of handyman_user
-- ----------------------------
INSERT INTO `handyman_user` VALUES (3, 'mohammad', 'sweedan', 'msyafa@gmail.com', '123456', 'mm');
INSERT INTO `handyman_user` VALUES (4, 'mohammad', 'sweedan', 'msyafa@gmail.com', '1234567', 'mm');
INSERT INTO `handyman_user` VALUES (5, 'خاخاخا', 'نةنةنة', 'mm@mm.com', '123655', 'nbn');

SET FOREIGN_KEY_CHECKS = 1;
