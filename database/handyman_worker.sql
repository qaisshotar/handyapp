/*
 Navicat Premium Data Transfer

 Source Server         : Flowers
 Source Server Type    : MySQL
 Source Server Version : 100237
 Source Host           : theflowersandgiftsshop.com:3306
 Source Schema         : flowers_alhaj

 Target Server Type    : MySQL
 Target Server Version : 100237
 File Encoding         : 65001

 Date: 05/04/2021 23:28:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for handyman_worker
-- ----------------------------
DROP TABLE IF EXISTS `handyman_worker`;
CREATE TABLE `handyman_worker`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `occupation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of handyman_worker
-- ----------------------------
INSERT INTO `handyman_worker` VALUES (1, 'كوكو', '123', 'مزارع', 'mm');
INSERT INTO `handyman_worker` VALUES (2, 'ةنهله', '5827755', 'نجار', 'bbvjj');

SET FOREIGN_KEY_CHECKS = 1;
