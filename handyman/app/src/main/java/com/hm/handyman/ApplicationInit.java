package com.hm.handyman;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;


public class ApplicationInit extends Application  {

	@Override
	public void onCreate() {
		super.onCreate();
		AndroidNetworking.initialize(getApplicationContext());
	}

}
