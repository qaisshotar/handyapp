package com.hm.handyman;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ChooserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chooser);


        ImageView user_img = findViewById(R.id.user_img);
        ImageView worker_img = findViewById(R.id.worker_img);
        TextView user = findViewById(R.id.user);
        TextView worker = findViewById(R.id.worker);


        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = null;
                if (getIntent().getStringExtra("type").equalsIgnoreCase("in")){
                    ii = new Intent(ChooserActivity.this, SignInUserActivity.class);
                    ii.putExtra("user_type", "user");
                } else {
                    ii = new Intent(ChooserActivity.this, SignUpUserActivity.class);
                    ii.putExtra("user_type", "user");
                }
                startActivity(ii);
            }
        });
        user_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = null;
                if (getIntent().getStringExtra("type").equalsIgnoreCase("in")){
                    ii = new Intent(ChooserActivity.this, SignInUserActivity.class);
                    ii.putExtra("user_type", "user");
                } else {
                    ii = new Intent(ChooserActivity.this, SignUpUserActivity.class);
                    ii.putExtra("user_type", "user");
                }
                startActivity(ii);
            }
        });

        worker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = null;
                if (getIntent().getStringExtra("type").equalsIgnoreCase("in")){
                    ii = new Intent(ChooserActivity.this, SignInWorkerActivity.class);
                    ii.putExtra("user_type", "worker");
                } else {
                    ii = new Intent(ChooserActivity.this, SignUpWorkerActivity.class);
                    ii.putExtra("user_type", "worker");
                }
                startActivity(ii);
            }
        });
        worker_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = null;
                if (getIntent().getStringExtra("type").equalsIgnoreCase("in")){
                    ii = new Intent(ChooserActivity.this, SignInWorkerActivity.class);
                    ii.putExtra("user_type", "worker");
                } else {
                    ii = new Intent(ChooserActivity.this, SignUpWorkerActivity.class);
                    ii.putExtra("user_type", "worker");
                }
                startActivity(ii);
            }
        });

    }
}