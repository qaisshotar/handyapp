package com.hm.handyman;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;

import org.json.JSONObject;

public class SignInUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        EditText phone = findViewById(R.id.phone);
        EditText password = findViewById(R.id.password);


        TextView sign_in = findViewById(R.id.sign_in);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(phone.getText())){
                    Toast.makeText(SignInUserActivity.this, "Please fill phone number",Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(password.getText())){
                    Toast.makeText(SignInUserActivity.this, "Please fill password",Toast.LENGTH_LONG).show();
                } else {
                    AndroidNetworking.post("https://theflowersandgiftsshop.com/projects/handyman/webservices/index.php")
                            .setOkHttpClient(Helper.enableTls12OnPreLollipop().build())
                            .addBodyParameter("action", "sign_in")
                            .addBodyParameter("phone", phone.getText().toString().trim())
                            .addBodyParameter("password", password.getText().toString().trim())
                            .addBodyParameter("type", getIntent().getStringExtra("user_type"))
                            .setPriority(Priority.HIGH)
                            .build()
                            .getAsString(new StringRequestListener() {
                                @Override
                                public void onResponse(String response) {
                                    if (response.equalsIgnoreCase("-2")){
                                        Toast.makeText(SignInUserActivity.this, "Invalid phone number or password",Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(SignInUserActivity.this, "Successful login",Toast.LENGTH_LONG).show();
                                    }
                                }
                                @Override
                                public void onError(ANError anError) {
                                }
                            });
                }
            }
        });

    }
}