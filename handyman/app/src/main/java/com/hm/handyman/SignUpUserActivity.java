package com.hm.handyman;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;

public class SignUpUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_user);

        EditText first_name = findViewById(R.id.first_name);
        EditText last_name = findViewById(R.id.last_name);
        EditText email = findViewById(R.id.email);
        EditText phone = findViewById(R.id.phone);
        EditText password = findViewById(R.id.password);

        TextView sign_up = findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> map = new HashMap<>();
                map.put("action","register_user");
                map.put("first_name",first_name.getText().toString().trim().toLowerCase());
                map.put("last_name",last_name.getText().toString().trim().toLowerCase());
                map.put("email",email.getText().toString().trim().toLowerCase());
                map.put("phone",phone.getText().toString().trim().toLowerCase());
                map.put("password",password.getText().toString().trim());

                AndroidNetworking.post("https://theflowersandgiftsshop.com/projects/handyman/webservices/index.php")
                        .setOkHttpClient(Helper.enableTls12OnPreLollipop().build())
                        .addBodyParameter(map)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String response) {
                                if (response.toString().trim().equalsIgnoreCase("-2")){
                                    Toast.makeText(SignUpUserActivity.this, "Phone number is already used",Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(SignUpUserActivity.this, "Successful registration",Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                            }
                        });
            }
        });


    }
}